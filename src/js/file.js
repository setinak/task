if (typeof web3 !== 'underfined') {
	web3 = new Web3(web3.currentProvider);
	console.log('Connected');
} else {
	web3 = new Wen3(new Web3.providers.HttpProvider('http://localhost:8080'));
	console.log('Connected');
}

// web3.eth.defaultAccount = web3.eth.accounts[0];

web3.eth.getAccounts(function(error, accounts) {
    if (!error) {
        web3.eth.getBalance(accounts[0], function(error, balance) {
          if (!error) {
            console.log(
              "Your account: " +
                accounts[0] +
                " has a balance of: " +
                balance.toNumber() / 1000000000000000000 +
                " Ether"
            );
          } else {
            console.error(error);
          }
        });
      } else {
        console.error(error);
      }
    });

let GameContract = web3.eth.contract([
	{
		"constant": false,
		"inputs": [
			{
				"name": "_address",
				"type": "address"
			},
			{
				"name": "_name",
				"type": "bytes16"
			},
			{
				"name": "_age",
				"type": "uint256"
			}
		],
		"name": "addHero",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "name",
				"type": "bytes16"
			},
			{
				"indexed": false,
				"name": "age",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "damage",
				"type": "uint256"
			}
		],
		"name": "heroInfo",
		"type": "event"
	}
]);

let Game = GameContract.at('0x9729ca72fd71f3d3e043184e1f7786e89511d37f');
console.log(Game);

let heroEvent = Game.heroInfo({},'latest');

heroEvent.watch(function (err, result) {
	if (result) {
		$("#loader").hide();
		$("#game").html('Имя героя:' + web3.toAscii(result.args.name) + '\t Возрас:'+ (result.args.age) + '\t Урон:' + (result.args.damage));
		console.log(result);
	} else {
		$("#loader").hide();
		console.log(err);
	}
});

// Game.getHero(web3.eth.defaultAccount, (err, result) => {
// 	if(result) {
// 		$("#loader").hide();
// 		$("#game").html('Имя героя:' + result[0] + '\t Возрас:'+ result[1] + '\t Урон:' + result[2]);
// 		console.log(result);
// 	} 
// 	else {
// 		console.log(err);
// 	}
// });

$("#button").click(function() {
	$("#loader").show();
	Game.addHero(web3.eth.defaultAccount, $("#name").val(), $("#age").val(), (err, res) => {
		if(!err) {
			console.log(res);
		} else {
			console.log(err);
			$("#loader").hide();
		} 
	});
});