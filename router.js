let express = require('express'),
    router = express.Router(),

    mainController = require('./app/controllers/main');

router.get('/', mainController.getMainPage);

module.exports = router;